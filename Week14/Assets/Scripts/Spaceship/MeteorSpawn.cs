﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawn : MonoBehaviour
{
    public float minSpawnDelay = 1f;

    public float maxSpawnDelay = 2f;

    public GameObject meteorPrefab; 
    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Spawn()
    {
        Vector3 spawnPos = new Vector2(Random.Range(-6, 6), 6f);
        Instantiate(meteorPrefab,spawnPos,Quaternion.identity);
        
        Invoke("Spawn" ,Random.Range(minSpawnDelay,maxSpawnDelay)); 
    }
}
